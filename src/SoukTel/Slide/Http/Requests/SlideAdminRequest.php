<?php

namespace SoukTel\Slide\Http\Requests;

use App\Http\Requests\Request as FormRequest;
use Illuminate\Http\Request;

class SlideAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $slide = $this->route('slide');

        if (is_null($slide)) {
            // Determine if the user is authorized to access slide module,
            return $request->user('admin.web')->canDo('slide.slide.view');
        }

        if ($request->isMethod('POST') || $request->is('*/create')) {
            // Determine if the user is authorized to create an entry,
            return $request->user('admin.web')->canDo('slide.slide.create');
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            // Determine if the user is authorized to update an entry,
            return $request->user('admin.web')->canDo('slide.slide.update');
        }

        if ($request->isMethod('DELETE')) {
            // Determine if the user is authorized to delete an entry,
            return $request->user('admin.web')->canDo('slide.slide.delete');
        }

        // Determine if the user is authorized to view the module.
        return $request->user('admin.web')->canDo('slide.slide.view');

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST')) {
            // validation rule for create request.
            return [
                'title' => 'required',
                'slider_id' => 'required',
            ];
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            // Validation rule for update request.
            return [
                'title' => 'required',
                'slider_id' => 'required',
            ];
        }

        // Default validation rule.
        return [

        ];
    }

}
