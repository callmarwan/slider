<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Http\Requests\SlideUserApiRequest;
use SoukTel\Slide\Interfaces\SlideRepositoryInterface;
use SoukTel\Slide\Models\Slide;

/**
 * User API controller class.
 */
class SlideUserApiController extends BaseController
{
    /**
     * Initialize slide controller.
     *
     * @param type SlideRepositoryInterface $slide
     *
     * @return type
     */
    protected $guard = 'api';

    public function __construct(SlideRepositoryInterface $slide)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:api');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
         $this->repository = $slide;
        parent::__construct();
    }

    /**
     * Display a list of slide.
     *
     * @return json
     */
    public function index(SlideUserApiRequest $request)
    {
        $slides  = $this->repository
            ->pushCriteria(new \Lavalite\Slide\Repositories\Criteria\SlideUserCriteria())
            ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SlideListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->all();
        $slides['code'] = 2000;
        return response()->json($slides) 
            ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display slide.
     *
     * @param Request $request
     * @param Model   Slide
     *
     * @return Json
     */
    public function show(SlideUserApiRequest $request, Slide $slide)
    {

        if ($slide->exists) {
            $slide         = $slide->presenter();
            $slide['code'] = 2001;
            return response()->json($slide)
                ->setStatusCode(200, 'SHOW_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }

    /**
     * Show the form for creating a new slide.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(SlideUserApiRequest $request, Slide $slide)
    {
        $slide         = $slide->presenter();
        $slide['code'] = 2002;
        return response()->json($slide)
            ->setStatusCode(200, 'CREATE_SUCCESS');
    }

    /**
     * Create new slide.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(SlideUserApiRequest $request)
    {
        try {
            $attributes             = $request->all();
            $attributes['user_id']  = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $slide          = $this->repository->create($attributes);
            $slide          = $slide->presenter();
            $slide['code']  = 2004;

            return response()->json($slide)
                ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
        }

    }

    /**
     * Show slide for editing.
     *
     * @param Request $request
     * @param Model   $slide
     *
     * @return json
     */
    public function edit(SlideUserApiRequest $request, Slide $slide)
    {
        if ($slide->exists) {
            $slide         = $slide->presenter();
            $slide['code'] = 2003;
            return response()-> json($slide)
                ->setStatusCode(200, 'EDIT_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }
    }

    /**
     * Update the slide.
     *
     * @param Request $request
     * @param Model   $slide
     *
     * @return json
     */
    public function update(SlideUserApiRequest $request, Slide $slide)
    {
        try {

            $attributes = $request->all();

            $slide->update($attributes);
            $slide         = $slide->presenter();
            $slide['code'] = 2005;

            return response()->json($slide)
                ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the slide.
     *
     * @param Request $request
     * @param Model   $slide
     *
     * @return json
     */
    public function destroy(SlideUserApiRequest $request, Slide $slide)
    {

        try {

            $t = $slide->delete();

            return response()->json([
                'message'  => trans('messages.success.delete', ['Module' => trans('slide.name')]),
                'code'     => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
