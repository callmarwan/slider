<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Slide\Http\Requests\SliderUserRequest;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use SoukTel\Slide\Models\Slider;

class SliderUserController extends BaseController
{
    

    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    protected $guard = 'web';

    /**
     * Initialize slider controller.
     *
     * @param type SliderRepositoryInterface $slider
     *
     * @return type
     */
    protected $home = 'home';

    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->repository = $slider;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(SliderUserRequest $request)
    {
        $this->repository->pushCriteria(new \Lavalite\Slide\Repositories\Criteria\SliderUserCriteria());
        $sliders = $this->repository->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();

        $this->theme->prependTitle(trans('slider.names').' :: ');

        return $this->theme->of('slide::user.slider.index', compact('sliders'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Slider $slider
     *
     * @return Response
     */
    public function show(SliderUserRequest $request, Slider $slider)
    {
        Form::populate($slider);

        return $this->theme->of('slide::user.slider.show', compact('slider'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(SliderUserRequest $request)
    {

        $slider = $this->repository->newInstance([]);
        Form::populate($slider);

        return $this->theme->of('slide::user.slider.create', compact('slider'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(SliderUserRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id();
            $attributes['user_type'] = user_type();
            $slider = $this->repository->create($attributes);

            return redirect(trans_url('/user/slide/slider'))
                -> with('message', trans('messages.success.created', ['Module' => trans('slider.name')]))
                -> with('code', 201);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Slider $slider
     *
     * @return Response
     */
    public function edit(SliderUserRequest $request, Slider $slider)
    {

        Form::populate($slider);

        return $this->theme->of('slide::user.slider.edit', compact('slider'))->render();
    }

    /**
     * Update the specified resource.
     *
     * @param Request $request
     * @param Slider $slider
     *
     * @return Response
     */
    public function update(SliderUserRequest $request, Slider $slider)
    {
        try {
            $this->repository->update($request->all(), $slider->getRouteKey());

            return redirect(trans_url('/user/slide/slider'))
                ->with('message', trans('messages.success.updated', ['Module' => trans('slider.name')]))
                ->with('code', 204);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Remove the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(SliderUserRequest $request, Slider $slider)
    {
        try {
            $this->repository->delete($slider->getRouteKey());
            return redirect(trans_url('/user/slide/slider'))
                ->with('message', trans('messages.success.deleted', ['Module' => trans('slider.name')]))
                ->with('code', 204);

        } catch (Exception $e) {

            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);

        }
    }
}
