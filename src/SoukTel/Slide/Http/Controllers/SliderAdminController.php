<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Slide\Http\Requests\SliderAdminRequest;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use SoukTel\Slide\Models\Slider;

/**
 * Admin web controller class.
 */
class SliderAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize slider controller.
     *
     * @param type SliderRepositoryInterface $slider
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
         $this->repository = $slider;
        parent::__construct();
    }

    /**
     * Display a list of slider.
     *
     * @return Response
     */
    public function index(SliderAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        if ($request->wantsJson()) {
            $sliders  = $this->repository
                ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SliderListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->orderBy('id', 'DESC');
                })->paginate($pageLimit);

            $sliders['recordsTotal']    = $sliders['meta']['pagination']['total'];
            $sliders['recordsFiltered'] = $sliders['meta']['pagination']['total'];
            $sliders['request']         = $request->all();
            return response()->json($sliders, 200);

        }

        $this   ->theme->prependTitle(trans('slider.names').' :: ');
        return $this->theme->of('slide::admin.slider.index')->render();
    }

    /**
     * Display slider.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return Response
     */
    public function show(SliderAdminRequest $request, Slider $slider)
    {
        if (!$slider->exists) {
            return response()->view('slide::admin.slider.new', compact('slider'));
        }

        Form::populate($slider);
        return response()->view('slide::admin.slider.show', compact('slider'));
    }

    /**
     * Show the form for creating a new slider.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(SliderAdminRequest $request)
    {

        $slider = $this->repository->newInstance([]);

        Form::populate($slider);

        return response()->view('slide::admin.slider.create', compact('slider'));

    }

    /**
     * Create new slider.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(SliderAdminRequest $request)
    {
        try {
            $attributes             = $request->all();
            $attributes['user_id']  = user_id('admin.web');
            $attributes['user_type'] = user_type();
            $slider          = $this->repository->create($attributes);

            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => trans('slider.name')]),
                'code'     => 204,
                'redirect' => trans_url('/admin/slide/slider/'.$slider->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
            ], 400);
        }
    }

    /**
     * Show slider for editing.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return Response
     */
    public function edit(SliderAdminRequest $request, Slider $slider)
    {
        Form::populate($slider);
        return  response()->view('slide::admin.slider.edit', compact('slider'));
    }

    /**
     * Update the slider.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return Response
     */
    public function update(SliderAdminRequest $request, Slider $slider)
    {
        try {

            $attributes = $request->all();

            $slider->update($attributes);

            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => trans('slider.name')]),
                'code'     => 204,
                'redirect' => trans_url('/admin/slide/slider/'.$slider->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/slide/slider/'.$slider->getRouteKey()),
            ], 400);

        }
    }

    /**
     * Remove the slider.
     *
     * @param Model   $slider
     *
     * @return Response
     */
    public function destroy(SliderAdminRequest $request, Slider $slider)
    {

        try {

            $t = $slider->delete();

            return response()->json([
                'message'  => trans('messages.success.deleted', ['Module' => trans('slider.name')]),
                'code'     => 202,
                'redirect' => trans_url('/admin/slide/slider/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/slide/slider/'.$slider->getRouteKey()),
            ], 400);
        }
    }
}
