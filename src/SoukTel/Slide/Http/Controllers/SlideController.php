<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use SoukTel\Slide\Interfaces\SlideRepositoryInterface;

class SlideController extends BaseController
{

    /**
     * Constructor.
     *
     * @param type \SoukTel\Slide\Interfaces\SlideRepositoryInterface $slide
     *
     * @return type
     */
    public function __construct(SlideRepositoryInterface $slide, SliderRepositoryInterface $slider)
    {
        $this->middleware('web');
        $this->setupTheme(config('theme.themes.public.theme'), config('theme.themes.public.layout'));
        $this->repository = $slide;
        $this->slider = $slider;
        parent::__construct();
    }

    /**
     * Show slide's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $this->theme->asset()->container('footer')->add('atvimg', 'packages/atvImg/atvImg-min.js');
        $this->theme->asset()->container('footer')->add('isotop', 'packages/isotope/isotope.pkgd.min.js');

        $slides = $this->repository
            ->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SlidePublicCriteria())
            ->scopeQuery(function ($query) {
                return $query->with('slider')->orderBy('title', 'ASC');
            })->all();

        $sliders = $this->slider
            ->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SliderPublicCriteria())
            ->scopeQuery(function ($query) {
                return $query->orderBy('name', 'ASC');
            })->all();

        return $this->theme->of('slide::public.slide.index', compact('slides', 'sliders'))->render();
    }

    /**
     * Show slide.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $slide = $this->repository->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->where('slug', $slug);
        })->first(['*']);
        $this->slider->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SliderPublicCriteria());
        $sliders = $this->slider->scopeQuery(function ($query) {
            return $query->orderBy('name', 'ASC');
        })->all();

        return $this->theme->of('slide::public.slide.show', compact('slide', 'sliders'))->render();
    }
}
