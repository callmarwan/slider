<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Http\Requests\SliderAdminApiRequest;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use SoukTel\Slide\Models\Slider;

/**
 * Admin API controller class.
 */
class SliderAdminApiController extends BaseController
{
    

/**
     * Initialize slider controller.
     *
     * @param type SliderRepositoryInterface $slider
     *
     * @return type
     */
    protected $guard = 'admin.api';

    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:admin.api');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $slider;
        parent::__construct();
    }

    /**
     * Display a list of slider.
     *
     * @return json
     */
    public function index(SliderAdminApiRequest $request)
    {
        $sliders  = $this->repository
            ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SliderListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->all();
        $sliders['code'] = 2000;
        return response()->json($sliders) 
                         ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display slider.
     *
     * @param Request $request
     * @param Model   Slider
     *
     * @return Json
     */
    public function show(SliderAdminApiRequest $request, Slider $slider)
    {
        $slider         = $slider->presenter();
        $slider['code'] = 2001;
        return response()->json($slider)
                         ->setStatusCode(200, 'SHOW_SUCCESS');;

    }

    /**
     * Show the form for creating a new slider.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(SliderAdminApiRequest $request, Slider $slider)
    {
        $slider         = $slider->presenter();
        $slider['code'] = 2002;
        return response()->json($slider)
                         ->setStatusCode(200, 'CREATE_SUCCESS');

    }

    /**
     * Create new slider.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(SliderAdminApiRequest $request)
    {
        try {
            $attributes             = $request->all();
            $attributes['user_id']  = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $slider          = $this->repository->create($attributes);
            $slider          = $slider->presenter();
            $slider['code']  = 2004;

            return response()->json($slider)
                             ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
;
        }
    }

    /**
     * Show slider for editing.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return json
     */
    public function edit(SliderAdminApiRequest $request, Slider $slider)
    {
        $slider         = $slider->presenter();
        $slider['code'] = 2003;
        return response()-> json($slider)
                        ->setStatusCode(200, 'EDIT_SUCCESS');;
    }

    /**
     * Update the slider.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return json
     */
    public function update(SliderAdminApiRequest $request, Slider $slider)
    {
        try {

            $attributes = $request->all();

            $slider->update($attributes);
            $slider         = $slider->presenter();
            $slider['code'] = 2005;

            return response()->json($slider)
                             ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the slider.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return json
     */
    public function destroy(SliderAdminApiRequest $request, Slider $slider)
    {

        try {

            $t = $slider->delete();

            return response()->json([
                'message'  => trans('messages.success.delete', ['Module' => trans('slider.name')]),
                'code'     => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
