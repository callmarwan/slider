<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use SoukTel\Slide\Repositories\Presenter\SliderItemTransformer;

/**
 * Pubic API controller class.
 */
class SliderApiController extends BaseController
{
   
    
    /**
     * Constructor.
     *
     * @param type \SoukTel\Slider\Interfaces\SliderRepositoryInterface $slider
     *
     * @return type
     */
    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->middleware('api');
        $this->repository = $slider;
        parent::__construct();
    }

    /**
     * Show slider's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $sliders = $this->repository
            ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SliderListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->paginate();

        $sliders['code'] = 2000;
        return response()->json($sliders)
                ->setStatusCode(200, 'INDEX_SUCCESS');
    }

    /**
     * Show slider.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $slider = $this->repository
            ->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        if (!is_null($slider)) {
            $slider         = $this->itemPresenter($module, new SliderItemTransformer);
            $slider['code'] = 2001;
            return response()->json($slider)
                ->setStatusCode(200, 'SHOW_SUCCESS');
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }
}
