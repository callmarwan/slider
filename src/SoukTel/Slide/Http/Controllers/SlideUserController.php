<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Slide\Http\Requests\SlideUserRequest;
use SoukTel\Slide\Interfaces\SlideRepositoryInterface;
use SoukTel\Slide\Models\Slide;

class SlideUserController extends BaseController
{

    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    protected $guard = 'web';

    /**
     * Initialize slide controller.
     *
     * @param type SlideRepositoryInterface $slide
     *
     * @return type
     */
    protected $home = 'home';

    public function __construct(SlideRepositoryInterface $slide)
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
         $this->repository = $slide;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(SlideUserRequest $request)
    {
        $this->repository->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SlideUserCriteria());
        $slides = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC');
        })->paginate();

        $this->theme->prependTitle(trans('slide.names') . ' :: ');

        return $this->theme->of('slide::user.slide.index', compact('slides'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Slide $slide
     *
     * @return Response
     */
    public function show(SlideUserRequest $request, Slide $slide)
    {
        Form::populate($slide);

        return $this->theme->of('slide::user.slide.show', compact('slide'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(SlideUserRequest $request)
    {

        $slide = $this->repository->newInstance([]);
        Form::populate($slide);

        return $this->theme->of('slide::user.slide.create', compact('slide'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(SlideUserRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id();
            $attributes['user_type'] = user_type();
            $slide = $this->repository->create($attributes);

            return redirect(trans_url('/user/slide/slide'))
                ->with('message', trans('messages.success.created', ['Module' => trans('slide.name')]))
                ->with('code', 201);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Slide $slide
     *
     * @return Response
     */
    public function edit(SlideUserRequest $request, Slide $slide)
    {

        Form::populate($slide);

        return $this->theme->of('slide::user.slide.edit', compact('slide'))->render();
    }

    /**
     * Update the specified resource.
     *
     * @param Request $request
     * @param Slide $slide
     *
     * @return Response
     */
    public function update(SlideUserRequest $request, Slide $slide)
    {
        try {
            $attributes = $request->all();
            $attributes['published'] = 'No';
            $this->repository->update($attributes, $slide->getRouteKey());
            
            return redirect(trans_url('/user/slide/slide'))
                ->with('message', trans('messages.success.updated', ['Module' => trans('slide.name')]))
                ->with('code', 204);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Remove the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(SlideUserRequest $request, Slide $slide)
    {
        try {
            $this->repository->delete($slide->getRouteKey());
            return response()->json([
                'message'  => trans('messages.success.deleted', ['Module' => trans('slide.name')]),
                'code'     => 202,
                'redirect' => trans_url('/user/slide/slide'),
            ], 201);

        } catch (Exception $e) {

            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);

        }
    }
}
