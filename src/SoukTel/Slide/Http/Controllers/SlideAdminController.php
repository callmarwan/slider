<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Slide\Http\Requests\SlideAdminRequest;
use SoukTel\Slide\Interfaces\SlideRepositoryInterface;
use SoukTel\Slide\Models\Slide;

/**
 * Admin web controller class.
 */
class SlideAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize slide controller.
     *
     * @param type SlideRepositoryInterface $slide
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(SlideRepositoryInterface $slide)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $slide;
        parent::__construct();
    }

    /**
     * Display a list of slide.
     *
     * @return Response
     */
    public function index(SlideAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');

        if ($request->wantsJson()) {
            $slides = $this->repository
                ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SlideListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->orderBy('title');
                })->paginate($pageLimit);

            $slides['recordsTotal'] = $slides['meta']['pagination']['total'];
            $slides['recordsFiltered'] = $slides['meta']['pagination']['total'];
            $slides['request'] = $request->all();
            return response()->json($slides, 200);

        }

        $this->theme->prependTitle(trans('slide.names') . ' :: ');
        return $this->theme->of('slide::admin.slide.index')->render();
    }

    /**
     * Display slide.
     *
     * @param Request $request
     * @param Model   $slide
     *
     * @return Response
     */
    public function show(SlideAdminRequest $request, Slide $slide)
    {

        if (!$slide->exists) {
            return response()->view('slide::admin.slide.new', compact('slide'));
        }

        Form::populate($slide);
        return response()->view('slide::admin.slide.show', compact('slide'));
    }

    /**
     * Show the form for creating a new slide.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(SlideAdminRequest $request)
    {

        $slide = $this->repository->newInstance([]);

        Form::populate($slide);

        return response()->view('slide::admin.slide.create', compact('slide'));

    }

    /**
     * Create new slide.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(SlideAdminRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            $attributes['user_type'] = user_type();
            $slide = $this->repository->create($attributes);

            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => trans('slide.name')]),
                'code'     => 204,
                'redirect' => trans_url('/admin/slide/slide/' . $slide->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }

    }

    /**
     * Show slide for editing.
     *
     * @param Request $request
     * @param Model   $slide
     *
     * @return Response
     */
    public function edit(SlideAdminRequest $request, Slide $slide)
    {
        Form::populate($slide);
        return response()->view('slide::admin.slide.edit', compact('slide'));
    }

    /**
     * Update the slide.
     *
     * @param Request $request
     * @param Model   $slide
     *
     * @return Response
     */
    public function update(SlideAdminRequest $request, Slide $slide)
    {
        try {

            $attributes = $request->all();
            $slide->update($attributes);

            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => trans('slide.name')]),
                'code'     => 204,
                'redirect' => trans_url('/admin/slide/slide/' . $slide->getRouteKey()),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/slide/slide/' . $slide->getRouteKey()),
            ], 400);

        }

    }

    /**
     * Remove the slide.
     *
     * @param Model   $slide
     *
     * @return Response
     */
    public function destroy(SlideAdminRequest $request, Slide $slide)
    {

        try {

            $t = $slide->delete();

            return response()->json([
                'message'  => trans('messages.success.deleted', ['Module' => trans('slide.name')]),
                'code'     => 202,
                'redirect' => trans_url('/admin/slide/slide/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/slide/slide/' . $slide->getRouteKey()),
            ], 400);
        }

    }

}
