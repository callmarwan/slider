<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Interfaces\SlideRepositoryInterface;
use SoukTel\Slide\Repositories\Presenter\SlideItemTransformer;

/**
 * Pubic API controller class.
 */
class SlideApiController extends BaseController
{
     /**
     * Constructor.
     *
     * @param type \SoukTel\Slide\Interfaces\SlideRepositoryInterface $slide
     *
     * @return type
     */
    public function __construct(SlideRepositoryInterface $slide)
    {
        $this->middleware('api');
         $this->repository = $slide;
        parent::__construct();
    }

    /**
     * Show slide's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $slides = $this->repository
            ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SlideListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->paginate();

        $slides['code'] = 2000;
        return response()->json($slides)
                ->setStatusCode(200, 'INDEX_SUCCESS');
    }

    /**
     * Show slide.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $slide = $this->repository
            ->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        if (!is_null($slide)) {
            $slide         = $this->itemPresenter($module, new SlideItemTransformer);
            $slide['code'] = 2001;
            return response()->json($slide)
                ->setStatusCode(200, 'SHOW_SUCCESS');
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }
}
