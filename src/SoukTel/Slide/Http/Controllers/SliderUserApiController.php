<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Http\Requests\SliderUserApiRequest;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use SoukTel\Slide\Models\Slider;

/**
 * User API controller class.
 */
class SliderUserApiController extends BaseController
{
    
    /**
     * Initialize slider controller.
     *
     * @param type SliderRepositoryInterface $slider
     *
     * @return type
     */
    protected $guard = 'api';

    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:api');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->repository = $slider;
        parent::__construct();
    }

    /**
     * Display a list of slider.
     *
     * @return json
     */
    public function index(SliderUserApiRequest $request)
    {
        $sliders  = $this->repository
            ->pushCriteria(new \Lavalite\Slide\Repositories\Criteria\SliderUserCriteria())
            ->setPresenter('\\SoukTel\\Slide\\Repositories\\Presenter\\SliderListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->all();
        $sliders['code'] = 2000;
        return response()->json($sliders) 
            ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display slider.
     *
     * @param Request $request
     * @param Model   Slider
     *
     * @return Json
     */
    public function show(SliderUserApiRequest $request, Slider $slider)
    {

        if ($slider->exists) {
            $slider         = $slider->presenter();
            $slider['code'] = 2001;
            return response()->json($slider)
                ->setStatusCode(200, 'SHOW_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }

    /**
     * Show the form for creating a new slider.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(SliderUserApiRequest $request, Slider $slider)
    {
        $slider         = $slider->presenter();
        $slider['code'] = 2002;
        return response()->json($slider)
            ->setStatusCode(200, 'CREATE_SUCCESS');
    }

    /**
     * Create new slider.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(SliderUserApiRequest $request)
    {
        try {
            $attributes             = $request->all();
            $attributes['user_id']  = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $slider          = $this->repository->create($attributes);
            $slider          = $slider->presenter();
            $slider['code']  = 2004;

            return response()->json($slider)
                ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
        }

    }

    /**
     * Show slider for editing.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return json
     */
    public function edit(SliderUserApiRequest $request, Slider $slider)
    {
        if ($slider->exists) {
            $slider         = $slider->presenter();
            $slider['code'] = 2003;
            return response()-> json($slider)
                ->setStatusCode(200, 'EDIT_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }
    }

    /**
     * Update the slider.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return json
     */
    public function update(SliderUserApiRequest $request, Slider $slider)
    {
        try {

            $attributes = $request->all();

            $slider->update($attributes);
            $slider         = $slider->presenter();
            $slider['code'] = 2005;

            return response()->json($slider)
                ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the slider.
     *
     * @param Request $request
     * @param Model   $slider
     *
     * @return json
     */
    public function destroy(SliderUserApiRequest $request, Slider $slider)
    {

        try {

            $t = $slider->delete();

            return response()->json([
                'message'  => trans('messages.success.delete', ['Module' => trans('slider.name')]),
                'code'     => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
