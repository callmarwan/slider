<?php

namespace SoukTel\Slide\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Slide\Interfaces\SliderRepositoryInterface;

class SliderController extends BaseController
{

    /**
     * Constructor.
     *
     * @param type \SoukTel\Slider\Interfaces\SliderRepositoryInterface $slider
     *
     * @return type
     */
    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->middleware('web');
        $this->setupTheme(config('theme.themes.public.theme'), config('theme.themes.public.layout'));
        $this->repository = $slider;
        parent::__construct();
    }

    /**
     * Show slider's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $sliders = $this->repository->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();

        return $this->theme->of('slide::public.slider.index', compact('sliders'))->render();
    }

    /**
     * Show slider.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $slider = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->theme->of('slide::public.slider.show', compact('slider'))->render();
    }
}
