<?php

// Admin web routes  for slide
Route::group(['prefix' => trans_setlocale() . '/admin/slide'], function () {
    Route::post('slide/status/{slide}', 'SoukTel\Slide\Http\Controllers\SlideAdminController@update');
    Route::resource('slide', 'SoukTel\Slide\Http\Controllers\SlideAdminController');
});

// Admin API routes  for slide
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/slide'], function () {
    Route::resource('slide', 'SoukTel\Slide\Http\Controllers\SlideAdminApiController');
});

// User web routes for slide
Route::group(['prefix' => trans_setlocale() . '/user/slide'], function () {
    Route::resource('slide', 'SoukTel\Slide\Http\Controllers\SlideUserController');
});

// User API routes for slide
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/slide'], function () {
    Route::resource('slide', 'SoukTel\Slide\Http\Controllers\SlideUserApiController');
});

// Public web routes for slide
Route::group(['prefix' => trans_setlocale() . '/slide'], function () {
    Route::get('/', 'SoukTel\Slide\Http\Controllers\SlideController@index');
    Route::get('{slug?}', 'SoukTel\Slide\Http\Controllers\SlideController@show');
});

// Public API routes for slide
Route::group(['prefix' => trans_setlocale() . 'api/v1/slides'], function () {
    Route::get('/', 'SoukTel\Slide\Http\Controllers\SlideApiController@index');
    Route::get('/{slug?}', 'SoukTel\Slide\Http\Controllers\SlideApiController@show');
});

// Admin web routes  for slider
Route::group(['prefix' => trans_setlocale() . '/admin/slide'], function () {
    Route::resource('slider', 'SoukTel\Slide\Http\Controllers\SliderAdminController');
});

// Admin API routes  for slider
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/slide'], function () {
    Route::resource('slider', 'SoukTel\Slide\Http\Controllers\SliderAdminApiController');
});

// User web routes for slider
Route::group(['prefix' => trans_setlocale() . '/user/slide'], function () {
    Route::resource('slider', 'SoukTel\Slide\Http\Controllers\SliderUserController');
});

// User API routes for slider
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/slide'], function () {
    Route::resource('slider', 'SoukTel\Slide\Http\Controllers\SliderUserApiController');
});

// Public web routes for slider
Route::group(['prefix' => trans_setlocale() . '/slide'], function () {
    Route::get('/slider', 'SoukTel\Slide\Http\Controllers\SliderController@index');
    Route::get('/slider/{slug?}', 'SoukTel\Slide\Http\Controllers\SliderController@show');
});

// Public API routes for slider
Route::group(['prefix' => trans_setlocale() . 'api/v1/slides'], function () {
    Route::get('/', 'SoukTel\Slide\Http\Controllers\SliderApiController@index');
    Route::get('/{slug?}', 'SoukTel\Slide\Http\Controllers\SliderApiController@show');
});
