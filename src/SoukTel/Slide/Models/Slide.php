<?php

namespace SoukTel\Slide\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Trans;
use Litepie\User\Traits\UserModel;

class Slide extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Trans, Revision, PresentableTrait, UserModel;

    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'package.slide.slide';

    /**
     * The slide_sliders that belong to the slide.
     */
    public function slider()
    {
        return $this->belongsTo('SoukTel\Slide\Models\Slider', 'slider_id');
    }
	
	public function getDefaultImage()
    {
        $image = json_decode($this->attributes['image']);
//        \Log::info(config('filer.folder', 'uploads'). '/' . $image->folder.'/'.$image->file);
        return config('filer.folder', 'uploads') . '/slide/slide/' . $image->folder . '/' . $image->file;
    }
}
