<?php

namespace SoukTel\Slide\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the package.
     *
     * @var array
     */
    protected $policies = [
        // Bind Slide policy
        \SoukTel\Slide\Models\Slide::class => \SoukTel\Slide\Policies\SlidePolicy::class,
// Bind Slider policy
        \SoukTel\Slide\Models\Slider::class => \SoukTel\Slide\Policies\SliderPolicy::class,
    ];

    /**
     * Register any package authentication / authorization services.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
    }
}
