<?php

namespace SoukTel\Slide\Providers;

use Illuminate\Support\ServiceProvider;

class SlideServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../../../resources/views', 'slide');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../../../resources/lang', 'slide');

        // Call pblish redources function
        $this->publishResources();

        include __DIR__ . '/../Http/routes.php';
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Bind facade
        $this->app->bind('slide', function ($app) {
            return $this->app->make('SoukTel\Slide\Slide');
        });

// Bind Slide to repository
        $this->app->bind(
            \SoukTel\Slide\Interfaces\SlideRepositoryInterface::class,
            \SoukTel\Slide\Repositories\Eloquent\SlideRepository::class
        );
        // Bind Slider to repository
        $this->app->bind(
            \SoukTel\Slide\Interfaces\SliderRepositoryInterface::class,
            \SoukTel\Slide\Repositories\Eloquent\SliderRepository::class
        );

        $this->app->register(\SoukTel\Slide\Providers\AuthServiceProvider::class);
        $this->app->register(\SoukTel\Slide\Providers\EventServiceProvider::class);
        $this->app->register(\SoukTel\Slide\Providers\RouteServiceProvider::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['slide'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../../../config/config.php' => config_path('package/slide.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../../../resources/views' => base_path('resources/views/vendor/slide')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../../../resources/lang' => base_path('resources/lang/vendor/slide')], 'lang');

        // Publish migrations
        $this->publishes([__DIR__ . '/../../../../database/migrations/' => base_path('database/migrations')], 'migrations');

        // Publish seeds
        $this->publishes([__DIR__ . '/../../../../database/seeds/' => base_path('database/seeds')], 'seeds');

        // Publish public
        $this->publishes([__DIR__ . '/../../../../public/' => public_path('/')], 'uploads');
    }
}
