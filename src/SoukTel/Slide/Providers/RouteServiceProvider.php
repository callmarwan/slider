<?php

namespace SoukTel\Slide\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use SoukTel\Slide\Models\Slide;
use Request;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'SoukTel\Slide\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param   \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        if (Request::is('*/slide/slide/*')) {
            $router->bind('slide', function ($id) {
                $slide = $this->app->make('\SoukTel\Slide\Interfaces\SlideRepositoryInterface');
                return $slide->findorNew($id);
            });
        }
if (Request::is('*/slide/slider/*')) {
            $router->bind('slider', function ($id) {
                $slider = $this->app->make('\SoukTel\Slide\Interfaces\SliderRepositoryInterface');
                return $slider->findorNew($id);
            });
        }

    }

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require __DIR__ . '/../Http/routes.php';
        });
    }
}
