<?php

namespace SoukTel\Slide\Repositories\Eloquent;

use SoukTel\Slide\Interfaces\SlideRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class SlideRepository extends BaseRepository implements SlideRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.slide.slide.search');
        return config('package.slide.slide.model');
    }
}
