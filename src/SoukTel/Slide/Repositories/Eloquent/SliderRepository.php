<?php

namespace SoukTel\Slide\Repositories\Eloquent;

use SoukTel\Slide\Interfaces\SliderRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class SliderRepository extends BaseRepository implements SliderRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.slide.slider.search');
        return config('package.slide.slider.model');
    }
}
