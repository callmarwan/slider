<?php

namespace SoukTel\Slide\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class SliderListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Slide\Models\Slider $slider)
    {
        return [
            'id'     => $slider->getRouteKey(),
            'name'   => ucfirst($slider->name),
            'status' => ucfirst($slider->status),
        ];
    }
}
