<?php

namespace SoukTel\Slide\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class SliderItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Slide\Models\Slider $slider)
    {
        return [
            'id'                => $slider->getRouteKey(),
            'name'              => $slider->name,
            'status'            => $slider->status,
        ];
    }
}