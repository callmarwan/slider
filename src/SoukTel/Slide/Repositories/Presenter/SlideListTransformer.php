<?php

namespace SoukTel\Slide\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class SlideListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Slide\Models\Slide $slide)
    {
        return [
            'id'          => $slide->getRouteKey(),
            'title'       => ucfirst($slide->title),
            'published'   => $slide->published,
            'details'     => ucfirst($slide->details),
            'slider_id' => ucfirst($slide->slider['name']),
            'image'       => $slide->image,
            'images'      => $slide->images,
            'status'      => ucfirst($slide->status),
             'published'    => ($slide->published == 'Yes') ? 'Published' : '-',
        ];
    }
}
