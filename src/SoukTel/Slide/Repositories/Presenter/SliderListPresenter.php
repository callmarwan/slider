<?php

namespace SoukTel\Slide\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class SliderListPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SliderListTransformer();
    }
}