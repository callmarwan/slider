<?php

namespace SoukTel\Slide\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class SlideItemPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SlideItemTransformer();
    }
}