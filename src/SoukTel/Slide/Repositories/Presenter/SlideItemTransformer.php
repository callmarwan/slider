<?php

namespace SoukTel\Slide\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class SlideItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Slide\Models\Slide $slide)
    {
        return [
            'id'          => $slide->getRouteKey(),
            'title'       => ucfirst($slide->title),
            'published'   => $slide->published,
            'details'     => ucfirst($slide->details),
            'slider_id' => $slide->slider_id,
            'image'       => $slide->image,
            'images'      => $slide->images,
            'status'      => $slide->status,
        ];
    }
}
