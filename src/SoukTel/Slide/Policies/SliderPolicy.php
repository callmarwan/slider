<?php

namespace SoukTel\Slide\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Slide\Models\Slider;

class SliderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the slider.
     *
     * @param User $user
     * @param Slider $slider
     *
     * @return bool
     */
    public function view(User $user, Slider $slider)
    {
        if ($user->canDo('slide.slider.view') && $user->is('admin')) {
            return true;
        }

        return $user->id === $slider->user_id;
    }

    /**
     * Determine if the given user can create a slider.
     *
     * @param User $user
     * @param Slider $slider
     *
     * @return bool
     */
    public function create(User $user)
    {
        return  $user->canDo('slide.slider.create');
    }

    /**
     * Determine if the given user can update the given slider.
     *
     * @param User $user
     * @param Slider $slider
     *
     * @return bool
     */
    public function update(User $user, Slider $slider)
    {
        if ($user->canDo('slide.slider.update') && $user->is('admin')) {
            return true;
        }

        return $user->id === $slider->user_id;
    }

    /**
     * Determine if the given user can delete the given slider.
     *
     * @param User $user
     * @param Slider $slider
     *
     * @return bool
     */
    public function destroy(User $user, Slider $slider)
    {
        if ($user->canDo('slide.slider.delete') && $user->is('admin')) {
            return true;
        }

        return $user->id === $slider->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
