<?php

namespace SoukTel\Slide\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Slide\Models\Slide;

class SlidePolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the slide.
     *
     * @param User $user
     * @param Slide $slide
     *
     * @return bool
     */
    public function view(User $user, Slide $slide)
    {
        if ($user->canDo('slide.slide.view') && $user->is('admin')) {
            return true;
        }

        return $user->id === $slide->user_id;
    }

    /**
     * Determine if the given user can create a slide.
     *
     * @param User $user
     * @param Slide $slide
     *
     * @return bool
     */
    public function create(User $user)
    {
        return  $user->canDo('slide.slide.create');
    }

    /**
     * Determine if the given user can update the given slide.
     *
     * @param User $user
     * @param Slide $slide
     *
     * @return bool
     */
    public function update(User $user, Slide $slide)
    {
        if ($user->canDo('slide.slide.update') && $user->is('admin')) {
            return true;
        }

        return $user->id === $slide->user_id;
    }

    /**
     * Determine if the given user can delete the given slide.
     *
     * @param User $user
     * @param Slide $slide
     *
     * @return bool
     */
    public function destroy(User $user, Slide $slide)
    {
        if ($user->canDo('slide.slide.delete') && $user->is('admin')) {
            return true;
        }

        return $user->id === $slide->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
