<?php

namespace SoukTel\Slide\Facades;

use Illuminate\Support\Facades\Facade;

class Slide extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'slide';
    }
}
