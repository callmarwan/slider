<?php

namespace SoukTel\Slide;

class Slide
{
    /**
     * $slide object.
     */
    protected $slide;
    /**
     * $slider object.
     */
    protected $slider;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Slide\Interfaces\SlideRepositoryInterface $slide,
        \SoukTel\Slide\Interfaces\SliderRepositoryInterface $slider) {
        $this->slide = $slide;
        $this->slider = $slider;
    }

    /**
     * Returns count of slide.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count($type)
    {
        $slides = $this->$type->all();
        return count($slides);
    }

    /**
     * Returns slider of slide.
     * @return array
     */
    public function getSlider()
    {
        $array = [];
        $sliders = $this->slider->scopeQuery(function ($query) {
            return $query->orderBy('name')->whereStatus('show');
        })->all();

        foreach ($sliders as $key => $slider) {
            $array[$slider['id']] = ucfirst($slider['name']);
        }

        return $array;

    }

    /**
     * get sliders for public side
     * @param type|string $view
     * @return type
     */
    public function viewSliders()
    {
        $this->slide->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SliderPublicCriteria());
        $sliders = $this->slider->scopeQuery(function ($query) {
            return $query->orderBy('name');
        })->all();

        return view('slide::public.slider.index', compact('sliders'))->render();
    }

    /**
     * get related slides
     * @param slider_id
     * @return array
     */
    public function getRelated($cid)
    {
        $this->slide->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SlidePublicCriteria());
        $slides = $this->slide->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereSlider_id($cid)->take(2);
        })->all();

        return $slides;
    }

    /**
     * get related slides
     * @param slider_id
     * @return array
     */
    public function recentSlide()
    {
        $this->slide->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SlidePublicCriteria());
        $slides = $this->slide->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC')->take(5);
        })->all();

        return $slides;
    }

    /**
     * get related slides
     * @param slider_id
     * @return array
     */
    public function getCount($cid)
    {
        $this->slide->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SlidePublicCriteria());
        $slides = $this->slide->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereSlider_id($cid);
        })->all();

        return count($slides);
    }

    /**
     * Returns count of slide.
     *
     * @param array $filter
     *
     * @return int
     */
    public function countSlides()
    {
        $this->slide->pushCriteria(new \SoukTel\Slide\Repositories\Criteria\SlidePublicCriteria());
        $slides = $this->slide->scopeQuery(function ($query) {
            return $query->whereStatus('Show');
        })->all();
        return count($slides);
    }

}
