<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'litecms',

    /*
     * Package.
     */
    'package'   => 'slide',

    /*
     * Modules.
     */
    'modules'   => [
        'slide',
        'slider',
    ],
    /*
     * Image size.
     */
    'image'     => [

        'sm' => [
            'width'     => '150',
            'height'    => '150',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'md' => [
            'width'     => '370',
            'height'    => '420',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'lg' => [
            'width'     => '430',
            'height'    => '430',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

    ],

    'slide' => [
        'model'         => 'SoukTel\Slide\Models\Slide',
        'table'         => 'slides',
        'presenter'     => \SoukTel\Slide\Repositories\Presenter\SlideItemPresenter::class,
        'hidden'        => [],
        'visible'       => [],
        'guarded'       => ['*'],
        'slugs'         => ['slug' => 'title'],
        'dates'         => ['deleted_at'],
        'appends'       => [],
        'fillable'      => ['user_id', 'user_type', 'upload_folder', 'title', 'details', 'slider_id', 'image', 'images', 'status'],
        'translate'     => [],

        'upload_folder' => '/slide/slide',
        'uploads'       => [
            'single'   => ['image'],
            'multiple' => ['images'],
        ],
        'casts'         => [
            'image'  => 'array',
            'images' => 'array',
        ],
        'revision'      => [],
        'perPage'       => '20',
        'search'        => [
            'title' => 'like',
            'status',
        ],
    ],
    'slider'  => [
        'model'         => 'SoukTel\Slide\Models\Slider',
        'table'         => 'slide_sliders',
        'presenter'     => \SoukTel\Slide\Repositories\Presenter\SliderItemPresenter::class,
        'hidden'        => [],
        'visible'       => [],
        'guarded'       => ['*'],
        'slugs'         => ['slug' => 'name'],
        'dates'         => ['deleted_at'],
        'appends'       => [],
        'fillable'      => ['user_id', 'user_type', 'name', 'status'],
        'translate'     => [],

        'upload_folder' => '/slide/slider',
        'uploads'       => [
            'single'   => ['image'],
            'multiple' => ['images'],
        ],
        'casts'         => [
            'image'  => 'array',
            'images' => 'array',
        ],
        'revision'      => [],
        'perPage'       => '20',
        'search'        => [
            'title' => 'like',
            'status',
        ],
    ],
];
