@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> {!! trans('slider.name') !!} <small> {!! trans('cms.manage') !!} {!! trans('slider.names') !!}</small>
@stop

@section('title')
{!! trans('slider.names') !!}
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! trans_url('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.dashboard') !!} </a></li>
    <li class="active">{!! trans('slider.names') !!}</li>
</ol>
@stop

@section('entry')
<div class="box box-warning" id='slide-slider-entry'>
</div>
@stop

@section('tools')
@stop

@section('content')
<table id="slide-slider-list" class="table table-striped table-bordered data-table">
    <thead  class="list_head">
        <th>{!! trans('slider.label.name')!!}</th>
        <th>{!! trans('slider.label.status')!!}</th>
    </thead>
    <thead  class="search_bar">
        <th>{!! Form::text('search[name]')->raw()!!}</th>
        <th>{!! Form::text('search[status]')->raw()!!}</th>
    </thead>
</table>
@stop

@section('script')
<script type="text/javascript">

var oTable;
$(document).ready(function(){
    app.load('#slide-slider-entry', '{!!trans_url('admin/slide/slider/0')!!}');
    oTable = $('#slide-slider-list').dataTable( {
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! trans_url('/admin/slide/slider') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $('#slide-slider-list .search_bar input, #slide-slider-list .search_bar select').each(
                function(){
                    aoData.push( { 'name' : $(this).attr('name'), 'value' : $(this).val() } );
                }
            );
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'name'},
                    {data :'status'},
        ],
        "pageLength": 25
    });

    $('#slide-slider-list tbody').on( 'click', 'tr', function () {

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');

        var d = $('#slide-slider-list').DataTable().row( this ).data();

        $('#slide-slider-entry').load('{!!trans_url('admin/slide/slider')!!}' + '/' + d.id);
    });

    $("#slide-slider-list .search_bar input, #slide-slider-list .search_bar select").on('keyup select', function (e) {
        e.preventDefault();
        console.log(e.keyCode);
        if (e.keyCode == 13 || e.keyCode == 9) {
            oTable.api().draw();
        }
    });
});
</script>
@stop

@section('style')
@stop

