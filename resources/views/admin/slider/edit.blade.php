<div class="box-header with-border">
    <h3 class="box-title"> Edit  {!! trans('slider.name') !!} [{!!$slider->name!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#slide-slider-edit'  data-load-to='#slide-slider-entry' data-datatable='#slide-slider-list'><i class="fa fa-floppy-o"></i> Save</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#slide-slider-entry' data-href='{{trans_url('admin/slide/slider')}}/{{$slider->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#slider" data-toggle="tab">{!! trans('slider.tab.name') !!}</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('slide-slider-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(trans_url('admin/slide/slider/'. $slider->getRouteKey()))!!}
        <div class="tab-content">
            <div class="tab-pane active" id="slider">
                @include('slide::admin.slider.partial.entry')
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
