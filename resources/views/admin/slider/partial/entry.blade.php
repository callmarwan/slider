<div class='col-md-6 col-sm-6'>
    {!! Form::text('name')
    ->required()
    -> label(trans('slider.label.name'))
    -> placeholder(trans('slider.placeholder.name'))!!}
</div>

<div class='col-md-6 col-sm-6'>
    {!! Form::select('status')
    -> options(trans('slider.status_options'))
    -> label(trans('slider.label.status'))
    -> placeholder(trans('slider.placeholder.status'))!!}
</div>
