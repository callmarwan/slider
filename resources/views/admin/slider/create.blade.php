<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.new') }}  {!! trans('slider.name') !!} </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#slide-slider-create'  data-load-to='#slide-slider-entry' data-datatable='#slide-slider-list'><i class="fa fa-floppy-o"></i> Save</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CLOSE' data-load-to='#slide-slider-entry' data-href='{{trans_url('admin/slide/slider/0')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.close') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Slider</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('slide-slider-create')
        ->method('POST')
        ->files('true')
        ->action(trans_url('admin/slide/slider'))!!}
        <div class="tab-content">
            <div class="tab-pane active" id="details">
                @include('slide::admin.slider.partial.entry')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
