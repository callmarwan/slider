@extends('admin::curd.index')
@section('heading')
    <i class="fa fa-file-text-o"></i> {!! trans('slide.name') !!}
    <small> {!! trans('cms.manage') !!} {!! trans('slide.names') !!}</small>
@stop

@section('title')
    {!! trans('slide.names') !!}
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! trans_url('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.dashboard') !!} </a></li>
        <li class="active">{!! trans('slide.names') !!}</li>
    </ol>
@stop

@section('entry')
    <div class="box box-warning" id='slide-slide-entry'>
    </div>
@stop

@section('tools')
@stop

@section('content')
    <table id="slide-slide-list" class="table table-striped table-bordered data-table">
        <thead class="list_head">
        <th>{!! trans('slide.label.title')!!}</th>
        <th>{!! trans('slide.label.slider_id')!!}</th>
        <th>{!! trans('slide.label.status')!!}</th>
        <th>{!! trans('published')!!}</th>
        </thead>
        <thead class="search_bar">
        <th>{!! Form::text('search[title]')->raw()!!}</th>
        <th>{!! Form::select('select[slider_id]')->raw()->options(['' => 'All'] + Slide::getSlider())!!}</th>
        <th>{!! Form::select('search[status]')->raw()->options(['' => 'All']+trans('slide.status_options'))!!}</th>
        <th>{!! Form::select('search[published]')
                ->options(['' => 'All','Yes' => 'Published', ' ' => 'Unpublished'])
                ->raw()!!}</th>
        </thead>
    </table>
@stop

@section('script')
    <script type="text/javascript">

        var oTable;
        $(document).ready(function () {
            app.load('#slide-slide-entry', '{!!trans_url('admin/slide/slide/0')!!}');
            oTable = $('#slide-slide-list').dataTable({
                "bProcessing": true,
                "sDom": 'R<>rt<ilp><"clear">',
                "bServerSide": true,
                "sAjaxSource": '{!! trans_url('/admin/slide/slide') !!}',
                "fnServerData": function (sSource, aoData, fnCallback) {

                    $('#slide-slide-list .search_bar input, #slide-slide-list .search_bar select').each(
                            function () {
                                aoData.push({'name': $(this).attr('name'), 'value': $(this).val()});
                            }
                    );
                    app.dataTable(aoData);
                    $.ajax({
                        'dataType': 'json',
                        'data': aoData,
                        'type': 'GET',
                        'url': sSource,
                        'success': fnCallback
                    });
                },

                "columns": [
                    {data: 'title'},
                    {data: 'slider_id'},
                    {data: 'status'},
                    {data: 'published'},
                ],
                "pageLength": 25
            });

            $('#slide-slide-list tbody').on('click', 'tr', function () {

                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                var d = $('#slide-slide-list').DataTable().row(this).data();

                $('#slide-slide-entry').load('{!!trans_url('admin/slide/slide')!!}' + '/' + d.id);
            });

            $("#slide-slide-list .search_bar input, #slide-slide-list .search_bar select").on('keyup select', function (e) {
                e.preventDefault();
                console.log(e.keyCode);
                if (e.keyCode == 13 || e.keyCode == 9) {
                    oTable.api().draw();
                }
            });
            $("#slide-slide-list .search_bar select, #published_on").on('change', function (e) {
                e.preventDefault();
                oTable.api().draw();
            });
        });
    </script>
@stop

@section('style')
@stop
