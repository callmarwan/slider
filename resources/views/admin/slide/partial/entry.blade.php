<div class="tab-content">
    <div class="tab-pane active" id="info">

        <div class='col-md-4 col-sm-6'>
            {!! Form::text('title')
            -> required()
            -> label(trans('slide.label.title'))
            -> placeholder(trans('slide.placeholder.title'))!!}
        </div>

        <div class='col-md-4 col-sm-6'>
            {!! Form::select('slider_id')
            -> required()
            -> options(Slide::getSlider())
            -> label(trans('slide.label.slider_id'))
            -> placeholder(trans('slide.placeholder.slider_id'))!!}
        </div>

        <div class='col-md-4 col-sm-6'>
            {!! Form::select('status')
            -> options(trans('slide.status_options'))
            -> label(trans('slide.label.status'))
            -> placeholder(trans('slide.placeholder.status'))!!}
        </div>

        <div class='col-md-12 col-sm-12'>
            {!! Form::textArea('details')
            -> addClass('html-editor')
            -> label(trans('slide.label.details'))
            -> placeholder(trans('slide.placeholder.details'))!!}
        </div>
    </div>
    <div class="tab-pane " id="image">
        <div class='col-md-6 col-sm-12'>
            <label>{!!trans('slide.label.image')!!}</label>
            {!! @$slide->fileUpload('image')!!}
            {!! @$slide->fileEdit('image')!!}
        </div>

        <div class='col-md-6 col-sm-12'>
            <label>Images</label>
            {!!@$slide->fileUpload('images')!!}
            {!!@$slide->fileEdit('images')!!}
        </div>
    </div>
</div>
