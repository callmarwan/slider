<div class="box-header with-border">
    <h3 class="box-title"> Edit  {!! trans('slide.name') !!} [{!!$slide->title!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#slide-slide-edit'  data-load-to='#slide-slide-entry' data-datatable='#slide-slide-list'><i class="fa fa-floppy-o"></i> Save</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#slide-slide-entry' data-href='{{trans_url('admin/slide/slide')}}/{{$slide->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#info" data-toggle="tab">Slide</a></li>
            <li ><a href="#image" data-toggle="tab">Image</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('slide-slide-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(trans_url('admin/slide/slide/'. $slide->getRouteKey()))!!}

                @include('slide::admin.slide.partial.entry')

        {!!Form::close()!!}
    </div>
</div>

