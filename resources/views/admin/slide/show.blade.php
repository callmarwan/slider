<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {!! trans('slide.name') !!}  [{!! $slide->title !!}]  </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#slide-slide-entry' data-href='{{trans_url('admin/slide/slide/create')}}'><i class="fa fa-times-circle"></i> New</button>
        @if($slide->id )
        @if($slide->published == 'Yes')
            <button type="button" class="btn btn-warning btn-sm" data-action="PUBLISHED" data-load-to='#slide-slide-entry' data-href="{{trans_url('admin/slide/slide/status/'. $slide->getRouteKey())}}" data-value="No" data-datatable='#slide-slide-list'><i class="fa fa-times-circle"></i> Suspend</button>
        @else
            <button type="button" class="btn btn-success btn-sm" data-action="PUBLISHED" data-load-to='#slide-slide-entry' data-href="{{trans_url('admin/slide/slide/status/'. $slide->getRouteKey())}}" data-value="Yes" data-datatable='#slide-slide-list'><i class="fa fa-check"></i> Published</button>
        @endif
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#slide-slide-entry' data-href='{{ trans_url('/admin/slide/slide') }}/{{$slide->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> Edit</button>
        <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#slide-slide-entry' data-datatable='#slide-slide-list' data-href='{{ trans_url('/admin/slide/slide') }}/{{$slide->getRouteKey()}}' >
        <i class="fa fa-times-circle"></i> Delete
        </button>
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#info" data-toggle="tab">Slide</a></li>
            <li ><a href="#image" data-toggle="tab">Image</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('slide-slide-show')
        ->method('POST')
        ->files('true')
        ->action(trans_url('admin/slide/slide'))!!}

                    @include('slide::admin.slide.partial.view')

        {!! Form::close() !!}
    </div>
</div>

