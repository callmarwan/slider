<div class="blog-detail-side-slider-wraper clearfix">
    <h3>Sliders</h3>
    <ul>
        <li class="{{ (Request::is('slide'))? 'active' : ''}}"><a href="{{trans_url('slide')}}">All</a><span class="cat-number">5</span></li>
        @forelse($sliders as  $slider)
        <li class="{{(Request::is('*slide/slider/'.$slider->slug))? 'active' : ''}}"><a href="{{trans_url('slide/slider')}}/{{@$slider->slug}}">{{$slider->name}}</a><span class="cat-number">4</span></li>
        @empty
        @endif

    </ul>
</div>
