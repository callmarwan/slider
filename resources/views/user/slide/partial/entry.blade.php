
<div class="row">
{!! Form::hidden('upload_folder')!!}
           <div class='col-md-4 col-sm-6'>
                   {!! Form::text('title')
                   -> required()
                   -> label(trans('slide.label.title'))
                   -> placeholder(trans('slide.placeholder.title'))!!}
           </div>

            <div class='col-md-4 col-sm-6'>
                   {!! Form::select('slider_id')
                   -> required()
                   -> options(Slide::getSlider())
                   -> label(trans('slide.label.slider_id'))
                   -> placeholder(trans('slide.placeholder.slider_id'))!!}
            </div>
            <div class='col-md-4 col-sm-6'>
                   {!! Form::select('status')
                   -> options(trans('slide.status_options'))
                   -> label(trans('slide.label.status'))
                   -> placeholder(trans('slide.placeholder.status'))!!}
            </div>

            <div class='col-md-12 col-sm-12'>
                   {!! Form::textArea('details')
                   -> addClass('html-editor')
                   -> label(trans('slide.label.details'))
                   -> placeholder(trans('slide.placeholder.details'))!!}
            </div>

            <div class='col-md-12 col-sm-12'>
            Image:
                {!!@$slide->fileUpload('image')!!}
                {!!@$slide->fileEdit('image')!!}
            </div>

            <div class='col-md-12 col-sm-12'>
            Images:
                {!!@$slide->fileUpload('images')!!}
                {!!@$slide->fileEdit('images')!!}
            </div>


</div>
