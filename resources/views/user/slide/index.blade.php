<div class="dashboard-content">
    <div class="panel panel-color panel-inverse">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-5 col-md-5">
                    <h3 class="panel-title">
                        {!! trans('slide.user_names') !!}
                    </h3>
                    <p class="panel-sub-title m-t-5 text-muted">
                       {!! trans('slide.user_name') !!}
                    </p>
                </div>
                <div class="col-sm-7 col-md-7">
                    <div class="row m-t-5">
                        <div class="col-xs-12 col-sm-7 m-b-5">
                                {!!Form::open()
                                ->method('GET')
                                ->action(URL::to('user/slide/slide'))!!}
                                <div class="input-group">
                                    {!!Form::text('search')->type('text')->class('form-control')->placeholder('Search for Slides')->raw()!!}
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-danger" type="button">
                                            <i class="icon-magnifier">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close()!!}
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <a class=" btn btn-success btn-block text-uppercase f-12" href="{{ trans_url('/user/slide/slide/create') }}">
                                {{ trans('cms.create')  }} Slide
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="panel-body" >
            @foreach($slides as $slide)
            <div class="popular-post-block" id="{!! $slide->getRouteKey() !!}">
                <div class="row">
                    <div class="dashboard-blog-pic">
                       
                        <img alt="" class="img-responsive" src="{!!url(@$slide->defaultImage('slide.sm','image'))!!}">
                            
                    </div>

                    <div class="dashboard-blog-desc popular-post-inner">
                        <div class="popular-post-desc">
                            <a href="{{ trans_url('/slide') }}/{!! $slide['slug']!!}">
                                <h4>
                                    {{ucfirst($slide['title'])}}
                                </h4>
                            </a>
                            <p>
                                {{ ucfirst($slide->slider['name'])}}
                            </p>
                        </div>
                    </div>
                    <div class="dashboard-blog-actions text-right">
                        <a class="btn btn-icon waves-effect btn-success m-b-5" href="{{trans_url('slide')}}/{{@$slide['slug']}}">
                            <i class="fa fa-eye">
                            </i>
                        </a>
                        <a class="btn btn-icon waves-effect btn-primary m-b-5" href="{{ trans_url('/user') }}/slide/slide/{!! $slide->getRouteKey() !!}/edit">
                            <i class="fa fa-pencil">
                            </i>
                        </a>
                         <a class="btn btn-icon waves-effect btn-danger" data-action="DELETE" data-href="{{ trans_url('/user/slide/slide') }}/{!! $slide->getRouteKey() !!}" data-remove="{!! $slide->getRouteKey() !!}">
                            <i class="fa fa-trash">
                            </i>
                        </a>
                    </div>
                </div>
            </div>
         @endforeach   
        </div>
        
    </div>
</div>
