				{!! Form::hidden('upload_folder')!!}
				<div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('slider.label.name'))
                       -> placeholder(trans('slider.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('status')
                       -> label(trans('slider.label.status'))
                       -> placeholder(trans('slider.placeholder.status'))!!}
                </div>

{!!   Form::actions()
->large_primary_submit('Submit')
->large_inverse_reset('Reset')
!!}
