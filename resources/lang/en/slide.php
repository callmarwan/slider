<?php

return [

    /**
     * Singlular and plural name of the module
     */
    'name' => 'Slide',
    'names' => 'Slides',
    'user_name' => 'My <span>Slide</span>',
    'user_names' => 'My <span>Slides</span>',
    'create' => 'Create My Slide',
    'edit' => 'Update My Slide',

    /**
     * Options for select/radio/check.
     */
    'status_options' => ['hide' => 'Hide', 'show' => 'Show'],

    /**
     * Placeholder for inputs
     */
    'placeholder' => [
        'title' => 'Please enter title',
        'details' => 'Please enter details',
        'slider_id' => 'Please select slider',
        'image' => 'Please enter image',
        'images' => 'Please enter images',
        'status' => 'Please select status',
    ],

    /**
     * Labels for inputs.
     */
    'label' => [
        'title' => 'Title',
        'details' => 'Details',
        'slider_id' => 'Slider',
        'image' => 'Image',
        'images' => 'Images',
        'status' => 'Status',
        'status' => 'Status',
        'created_at' => 'Created at',
        'updated_at' => 'Updated at',
    ],

    /**
     * Tab labels
     */
    'tab' => [
        'name' => 'Name',
    ],

    /**
     * Texts  for the module
     */
    'text' => [
        'preview' => 'Click on the below list for preview',
    ],
];
