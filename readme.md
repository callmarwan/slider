This is a Laravel 5 package that provides slide management facility for lavalite framework.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `litecms/slide`.

    "litecms/slide": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

```php
SoukTel\Slide\Providers\SlideServiceProvider::class,

```

And also add it to alias

```php
'Slide'  => SoukTel\Slide\Facades\Slide::class,
```

Use the below commands for publishing

Migration and seeds

    php artisan vendor:publish --provider="SoukTel\Slide\Providers\SlideServiceProvider" --tag="migrations"
    php artisan vendor:publish --provider="SoukTel\Slide\Providers\SlideServiceProvider" --tag="seeds"

Configuration

    php artisan vendor:publish --provider="SoukTel\Slide\Providers\SlideServiceProvider" --tag="config"

Language files

    php artisan vendor:publish --provider="SoukTel\Slide\Providers\SlideServiceProvider" --tag="lang"

Views files

    php artisan vendor:publish --provider="SoukTel\Slide\Providers\SlideServiceProvider" --tag="views"
    

Public folders

    php artisan vendor:publish --provider="SoukTel\Slide\Providers\SlideServiceProvider" --tag="public"

## Usage


